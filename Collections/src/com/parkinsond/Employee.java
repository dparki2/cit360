package com.parkinsond;

import java.util.Comparator;
import java.util.Objects;

public class Employee implements Comparator<Employee> {

    private String first_name;
    private String last_name;
    private String country;
    private String employee_code;

    public Employee(String first_name, String last_name, String country, String employee_code) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.country = country;
        this.employee_code = employee_code;
    }

    public Employee() {
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmployee_code() {
        return employee_code;
    }

    public void setEmployee_code(String employee_code) {
        this.employee_code = employee_code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(employee_code);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Employee))
            return false;
        Employee other = (Employee) obj;
        return Objects.equals(employee_code, other.employee_code);
    }

    @Override
    public int compare(Employee arg0, Employee arg1) {
        String descriptionA=arg0.getEmployee_code();

        String descriptionB=arg1.getEmployee_code();

        return descriptionA.compareTo(descriptionB);
    }

}
