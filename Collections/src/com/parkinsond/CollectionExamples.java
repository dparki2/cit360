package com.parkinsond;

import java.util.*;
import java.util.Map.Entry;

public class CollectionExamples {
    public static void main(String[] args) {

        // Setup a list of employees to be able to use in all methods
        Employee emp1 = new Employee ("Drew", "Parkinson", "USA", "001");
        Employee emp2 = new Employee ("Chris", "Taylor", "USA", "002");
        Employee emp3 = new Employee ("Ron", "Dennis", "Mexico", "003");
        Employee emp4 = new Employee ("Tracie", "Wagner", "Mexico", "004");
        Employee emp5 = new Employee ("James", "Ralstin", "Canada", "005");
        Employee emp6 = new Employee ("Mark", "Heppner", "Canada", "006");
        Employee emp7 = new Employee ("Ryan", "Alward", "USA", "007");
        Employee emp8 = new Employee("Sara", "Cope", "Canada", "008");

        // HashSet examples
        hashSetExample(emp1, emp2, emp3, emp4, emp5, emp6, emp7, emp8);

        // HashMap examples:
        hashMapExample(emp1, emp2, emp3, emp4, emp5, emp6, emp7);

        // Linked List example
        linkedListExample(emp1, emp2, emp3, emp4, emp5, emp6, emp7, emp8);

        // ArrayList examples
        arrayListExample(emp1, emp2, emp3, emp4, emp5, emp6, emp7, emp8);

        // Queue example
        queueExample(emp1, emp2, emp3, emp4, emp5, emp6, emp7);

        // Tree Example
        treeSetExample(emp1, emp2, emp3, emp4, emp5, emp6, emp7);

        // TreeMap Example
        treeMapExample(emp1, emp2, emp3, emp4, emp5, emp6, emp7);

    }

    private static void treeMapExample(Employee emp1, Employee emp2, Employee emp3, Employee emp4, Employee emp5, Employee emp6, Employee emp7) {
        TreeMap<String, Employee> workersTreeMap = new TreeMap<>();

        workersTreeMap.put("001", emp1);
        workersTreeMap.put("002", emp2);
        workersTreeMap.put("003", emp3);
        workersTreeMap.put("004", emp4);
        workersTreeMap.put("005", emp5);
        workersTreeMap.put("006", emp6);
        workersTreeMap.put("007", emp7);

        System.out.println("--- Use a TreeMap ---");
        System.out.println("Total Employees: " + workersTreeMap.size());

        Iterator<String> itMap = workersTreeMap.keySet().iterator();

        System.out.println("--- Use an Iterator to access the TreeMap keys ---");
        while(itMap.hasNext()){
            Object key = itMap.next();
            System.out.println("Info: " + key + " is Key: " + workersTreeMap.get(key).getLast_name()+ " is the Value ");
        }

        System.out.println("\n-------Various methods of Tree Maps-------");
        System.out.println("This shows the number of elements in the Tree Map: "+workersTreeMap.size());
        System.out.println("Here, we are checking to see if the Tree Map is Empty: "+workersTreeMap.isEmpty());
        System.out.println("Key 001 element of the Tree Map: "+workersTreeMap.get("001").getLast_name());
        System.out.println("Removed the element of the TreeMap with the key 003: "+workersTreeMap.remove("003").getLast_name());
        System.out.println("A null value happens if we want to get the key we just removed: "+workersTreeMap.get("003"));
        System.out.println("And now we check if there is an element with the key 003: "+workersTreeMap.containsKey("003"));
        System.out.println("Now we clear the elements of the Map to remove all entries: ");
        workersTreeMap.clear();
        System.out.println("Our check if we have removed it by checking its size: "+workersTreeMap.size());
        System.out.println("Finally, we verify if the TreeMap is empty: "+workersTreeMap.isEmpty());
    }

    private static void treeSetExample(Employee emp1, Employee emp2, Employee emp3, Employee emp4, Employee emp5, Employee emp6, Employee emp7) {
        Employee compareEmployees = new Employee();

        TreeSet <Employee> workersTreeSet = new TreeSet <>(compareEmployees);

        workersTreeSet.add(emp6);
        workersTreeSet.add(emp2);
        workersTreeSet.add(emp3);
        workersTreeSet.add(emp4);
        workersTreeSet.add(emp5);
        workersTreeSet.add(emp1);
        workersTreeSet.add(emp7);

        System.out.println("--- Use a TreeSet ---");
        for(Employee employ: workersTreeSet) {
            System.out.println(employ.getFirst_name());
        }

        // Check to see if the Tree is empty or not
        if (workersTreeSet.isEmpty()) {
            System.out.print("\nTree Set is empty.");
        } else {
            System.out.println("\nTree Set size: " + workersTreeSet.size()+" elements\n");
        }

        System.out.print("\nAccessing the first or last elements in a Tree Set is relatively fast.");
        // Retrieve first data from tree set
        System.out.println("\nFirst Employee: " + workersTreeSet.first().getFirst_name());

        // Retrieve last data from tree set
        System.out.println("\nLast Employee: " + workersTreeSet.last().getFirst_name());

        // We will remove the data from the tree set.
        if (workersTreeSet.remove(emp6)) { // remove element by value
            System.out.println("\nRemoved " + emp6.getFirst_name() + " from tree set");
        } else {
            System.out.println("\nData doesn't exist!");
        }

        if (workersTreeSet.isEmpty()) {
            System.out.print("\nTree Set is empty.");
        } else {
            System.out.println("\nNow the tree set contain: " + workersTreeSet.size()+" elements\n");
        }

        for(Employee employ: workersTreeSet) {
            System.out.println(employ.getFirst_name());
        }

        // Remove all the data in the tree set.
        workersTreeSet.clear();
        if (workersTreeSet.isEmpty()) {
            System.out.print("\nTree Set is now empty.");
        } else {
            System.out.println("\nTree Set size: " + workersTreeSet.size());

        }
    }

    private static void queueExample(Employee emp1, Employee emp2, Employee emp3, Employee emp4, Employee emp5, Employee emp6, Employee emp7) {
        Queue<Employee> workersQueue = new LinkedList<>();

        workersQueue.add(emp1);
        workersQueue.add(emp2);
        workersQueue.add(emp3);
        workersQueue.add(emp4);
        workersQueue.add(emp5);
        workersQueue.add(emp6);
        workersQueue.add(emp7);

        System.out.println("--- Use a Queue ---");
        // using Iterator to iterate through a queue
        Iterator<Employee> itr = workersQueue.iterator();

        // hasNext() returns true if the queue has more elements
        while (itr.hasNext())
        {
            // next() returns the next element in the iteration
            System.out.println(itr.next().getFirst_name());
        }

        System.out.println("Notice how the removal of an item is in the exact same order they were added\n");

        //Remove items, dequeue
        while (!workersQueue.isEmpty()) {
            System.out.println("Removing the following worker from the queue: " + workersQueue.peek().getFirst_name());
            workersQueue.remove();

        }
    }

    private static void arrayListExample(Employee emp1, Employee emp2, Employee emp3, Employee emp4, Employee emp5, Employee emp6, Employee emp7, Employee emp8) {
        List<Employee> workersArray = new ArrayList<>();

        workersArray.add(emp1);
        workersArray.add(emp2);
        workersArray.add(emp3);
        workersArray.add(emp4);
        workersArray.add(emp5);
        workersArray.add(emp6);
        workersArray.add(emp7);

        System.out.println("--- Use an ArrayList ---");
        //Print elements of the set
        System.out.println("Total List Objects: " + workersArray.size() + "\n");

        System.out.println("The workers employed are:");

        for (Employee worker : workersArray) {
            System.out.println(worker.getFirst_name() + " " + worker.getLast_name() + " from " + worker.getCountry());
        }

        //Modify the list and places it to a specific index so we can see the new place of the objects.
        workersArray.add(1, emp8);

        System.out.println("\nTotal List objects after add: " + workersArray.size() + "\n");

        for (Employee worker : workersArray) {
            System.out.println(worker.getFirst_name() + " " + worker.getLast_name() + " from " + worker.getCountry());
        }

        //Modify the list using set method. This will replace what is currently in the index.
        workersArray.set(2, emp1);

        System.out.println("\n After modified List Objects: " + workersArray.size() + "\n");

        for (Employee worker : workersArray) {
            System.out.println(worker.getFirst_name() + " " + worker.getLast_name() + " from " + worker.getCountry());
        }
    }

    private static void linkedListExample(Employee emp1, Employee emp2, Employee emp3, Employee emp4, Employee emp5, Employee emp6, Employee emp7, Employee emp8) {
        List<Employee> workersLinked = new LinkedList<>();

        workersLinked.add(emp1);
        workersLinked.add(emp2);
        workersLinked.add(emp3);
        workersLinked.add(emp4);
        workersLinked.add(emp5);
        workersLinked.add(emp6);
        workersLinked.add(emp7);

        System.out.println("--- Use an LinkedList ---");

        System.out.println("\nTotal Linked List objects: " + workersLinked.size() + "\n");

        ListIterator<Employee> it=workersLinked.listIterator();

        // hasNext() returns true if the queue has more elements
        while (it.hasNext())
        {
            // next() returns the next element in the iteration
            System.out.println(it.next().getFirst_name());
        }

        it.add(emp8);
        System.out.println("\nTotal Linked List objects after adding one employee: " + workersLinked.size() + "\n");

        for (Employee worker : workersLinked) {
            System.out.println(worker.getFirst_name() + " " + worker.getLast_name() + " from " + worker.getCountry());

        }

        workersLinked.remove(emp3);
        System.out.println("\nTotal Linked List objects after removing one employee: " + workersLinked.size() + "\n");

        for (Employee worker : workersLinked) {
            System.out.println(worker.getFirst_name() + " " + worker.getLast_name() + " from " + worker.getCountry());

        }
    }

    private static void hashMapExample(Employee emp1, Employee emp2, Employee emp3, Employee emp4, Employee emp5, Employee emp6, Employee emp7) {
        Map<String, Employee> workersMap = new HashMap<>();
        System.out.println("--- Use a HashMap ---");

        System.out.println("--- HashMap uses a Key, Value pair ---");
        workersMap.put("Drew", emp1);
        workersMap.put("Chris", emp2);
        workersMap.put("Ron", emp3);
        workersMap.put("Tracie", emp4);
        workersMap.put("James", emp5);
        workersMap.put("Mark", emp6);
        workersMap.put("Ryan", emp7);



        System.out.println("Total Employees: " + workersMap.size());

        for (Entry<String, Employee> employee : workersMap.entrySet()) {
            String employeeKey = employee.getKey();
            Employee employeeValue = employee.getValue();
            System.out.println(employeeKey+"  =  "+employeeValue.getLast_name());
        }

        String searchKey = "Ron";
        System.out.println("--- HashMap search is fast when you know the key ---");
        System.out.println("Find employee with the key: Ron\n");

        if(workersMap.containsKey(searchKey))
            System.out.println("Found a match with the Key " + searchKey + "\n");

        System.out.println("--- Remove Map elements all at once by using clear method ---");
        workersMap.clear();

        //Checking the number of elements
        System.out.println("You have: " + workersMap.size() + " worker elements\n ");
    }

    private static void hashSetExample(Employee emp1, Employee emp2, Employee emp3, Employee emp4, Employee emp5, Employee emp6, Employee emp7, Employee emp8) {

        Set<Employee> workersSet = new HashSet<>();

        workersSet.add(emp1);
        workersSet.add(emp2);
        workersSet.add(emp3);
        workersSet.add(emp4);
        workersSet.add(emp5);
        workersSet.add(emp6);
        workersSet.add(emp7);
        workersSet.add(emp8);

        System.out.println("--- Use a HashSet ---");
        System.out.println("\nWorkers Hash Set is empty: " + workersSet.isEmpty() + "\n");

        //Check the number of elements
        System.out.println("You have: " + workersSet.size() + " worker elements\n");

        // Display the workers
        System.out.println("The workers employed are:");

        System.out.println("--- HashSet elements are not guaranteed to be in a particular order ---");
        for (Employee worker : workersSet) {
            System.out.println(worker.getFirst_name() + " " + worker.getLast_name() + " from " + worker.getCountry());
        }

        Iterator<Employee> it= workersSet.iterator();

        System.out.println("--- Using Employee objects in this case preserved the insertion order ---");
        // hasNext() returns true if the queue has more elements
        while (it.hasNext())
        {
            // next() returns the next element in the iteration
            System.out.println(it.next().getFirst_name());
        }
        System.out.println("--- Remove HashSet elements all at once by using clear method ---");
        //Clear all elements in the collection
        workersSet.clear();

        // Show that the collection is empty
        System.out.println("\nWorkers Hash Set is empty: " + workersSet.isEmpty() + "\n");

        //Checking the number of elements
        System.out.println("You have: " + workersSet.size() + " worker elements\n");
    }
}
