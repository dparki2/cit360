package com.parkinsond;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadOrchestration {

    public static void main(String[] args) {

        int numOfVideos = 0;
        int numOfDevices = 1;

        explainVideoService();
        numOfVideos = promptUserForNumOfVideos();
        numOfDevices = promptForNumberOfDevices();
        downloadVideoSelections(numOfVideos, numOfDevices);

    }

    public static void explainVideoService() {

        //Purpose of this program
        System.out.println("This program will ask the user to download certain videos based on available devices.\n" +
                "Choosing an larger number of videos will result in an increase of download time \n" +
                "A larger number of devices will result in an increased amount of download streams on\n" +
                "your Internet connection.\n");
    }

    public static int promptUserForNumOfVideos() {

        Scanner input = new Scanner(System.in);
        int numVideos = 0;

        boolean correctInput = false;
        System.out.println("Please enter the number of videos you would like to download: ");
        do {
            try {
                numVideos = input.nextInt();
                if (numVideos > 0) {
                    correctInput = true;
                } else {
                    System.out.println("Invalid Input. Please enter a number greater than 0");
                    input.nextLine();
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid Input. Please enter a whole number: ");
                input.nextLine();
            }
        } while (!correctInput);

        return numVideos;
    }

    public static void downloadVideoSelections(int numVideos, int numDevices) {
        Scanner input = new Scanner(System.in);
        int videoSelector = 1;
        boolean correctInput = false;
        final String[] videoSelections = {"Ratatouille", "Cars", "Up", "Brave", "OnWard"};
        List<Runnable> videoDownloads = new ArrayList<Runnable>();

        //Gather input from user
        System.out.println("Enter '1' for Ratatouille, '2' for Cars, '3' for Up, '4' for Brave" +
                " or '5' for OnWard.");

        for (int i = 1; i <= numVideos; i++) {
            //Reset Boolean
            correctInput = false;

            //User correctly enters which video
            System.out.println("What should video " + i + " be?");
            do {
                try {
                    videoSelector = input.nextInt();
                    if (videoSelector > 0 && videoSelector <= 5) {
                        correctInput = true;
                    } else {
                        System.out.println("Invalid Input. Please enter a number between 1 and 5: ");
                        input.nextLine();
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Invalid Input. Please enter a whole number between 1 and 5: ");
                    input.nextLine();
                }
            } while (!correctInput);

            //Create Runnable VideoDownload objects (Step 1)
            videoDownloads.add(new VideoDownload(videoSelections[videoSelector - 1]));
        }

        /*
         Initiate ExecutorService implementation with user determined number of threads
         as the fixed pool size(Step 2)
        */
        ExecutorService videoService = Executors.newFixedThreadPool(numDevices);

        /*
        Execute threads by passing
        the VideoDownloads objects to the executor thread pool to execute (Step 3)
        */
        for (int i = 0 ; i < videoDownloads.size(); i++)
            videoService.execute(videoDownloads.get(i));

        // pool shutdown ( Step 4)
        videoService.shutdown();

    }

    public static int promptForNumberOfDevices()
    {
        Scanner input = new Scanner(System.in);
        boolean correctInput = false;
        int numOfDevices = 0;

        //User correctly enters number of devices
        System.out.println("How many devices will you use?");
        do {
            try {
                numOfDevices = input.nextInt();
                if (numOfDevices > 0) {
                    correctInput = true;
                } else {
                    System.out.println("Invalid Input. Please enter a whole number greater than zero: ");
                    input.nextLine();
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid Input. Please enter a whole number: ");
                input.nextLine();
            }
        } while (!correctInput);

        return  numOfDevices;
    }
}
