package com.parkinsond;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VideoDownload implements Runnable{

    private String name;
    private int currentDownloadSize = 0;
    private int downloadSize;
    private int downloadRate;

    public  VideoDownload(String videoName){
        switch (videoName){
            case "Ratatouille":
                name = videoName;
                downloadSize = 50;
                downloadRate = 7;
                break;
            case "Cars" :
                name = videoName;
                downloadSize = 42;
                downloadRate = 6;
                break;
            case "Up" :
                name = videoName;
                downloadSize = 45;
                downloadRate = 10;
                break;
            case "Brave" :
                name = videoName;
                downloadSize = 52;
                downloadRate = 12;
                break;
            case "OnWard" :
                name = videoName;
                downloadSize = 44;
                downloadRate = 11;
                break;
        }
    }
    @Override
    public void run() {

        try
        {
            System.out.println(name + " video download has started");
            Date d = new Date();
            int numOfSeconds = 0;
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
            System.out.println("Initialization Time for"
                    + " task name - "+ name +" = " +ft.format(d));
            //prints the initialization time for every task

            do{
                currentDownloadSize = currentDownloadSize +downloadRate;
                ++numOfSeconds;
                //Display to user current download size of video
                System.out.println(name + " has downloaded " + currentDownloadSize + " GB");

                System.out.println("Execution Time for - "+
                        name +" = " + numOfSeconds + " seconds");
                // prints the total execution time for every task

                Thread.sleep(1000);
            } while (currentDownloadSize < downloadSize);

            System.out.println(name + " video download has finished");
            System.out.println(name + " video download finished in " + numOfSeconds + " seconds");
            System.out.println(name + " download was " + currentDownloadSize + " GB in size and is ready to be viewed!");
        }
        catch (InterruptedException e) {
            e.printStackTrace();;
        }
    }
}
