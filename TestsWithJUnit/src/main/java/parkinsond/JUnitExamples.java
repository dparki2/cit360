package parkinsond;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JUnitExamples {

    /**
     * assertEquals test
     * addNumbers
     *
     * This block will return with the sum of two numbers as an integer
     **/
    public int sumOfTwoNumbers(int number1, int number2) {
        return number1 + number2;
    }

    /**
     * assertTrue
     * lessThan
     *
     * Here we are making sure that number one is
     * less than number two. If is is the case then
     * the method will return true. If number 1 is
     * not less than number 2 then the method will
     * return false.
     **/
    public boolean numberIsLessThan(int number1, int number2) {
        if (number1<number2){
            return true;
        } else {
            return false;
        }
    }

    /**
     * assertArrayEquals
     * oregonCities
     *
     * Here we are creating an ArrayList of multiple
     * cities in the State of Oregon. After the list is
     * created, we will return the contents.
     **/
    public List OregonCityArray(){
        List<String> oregonCities= new ArrayList<>();
        oregonCities.add("Portland");
        oregonCities.add("Newberg");
        oregonCities.add("Salem");
        oregonCities.add("Bend");
        oregonCities.add("Gresham");
        oregonCities.add("Tigard");
        return oregonCities;
    }

    /**
     * assertFalse
     * greaterThan
     *
     * This method is set up to be tested by JUnit in order to make sure that
     * number1 is greater than number2, and that the return is true. If not,
     * then method will returns false.
     **/
    public boolean numberIsGreaterThan(int number1, int number2) {
        if (number1 > number2){
            return true;
        }else {
            return false;
        }
    }

    /**
     * assertNotNull
     * DoctorWhos
     *
     * This block is a hash map of all the Doctor Whos and the
     * order they appear. The order of the Doctor is the acting key in this hash map when it
     * comes to testing.
     **/
    public String TheDoctors(final String key) {

        Map<String, String> DoctorWhos=new HashMap<>();
        DoctorWhos.put("1st Doctor","William Hartnell");
        DoctorWhos.put("2nd Doctor","Patrick Troughton");
        DoctorWhos.put("3rd Doctor","Jon Pertwee");
        DoctorWhos.put("4th Doctor","Tom Baker");
        DoctorWhos.put("5th Doctor","Peter Davison");
        DoctorWhos.put("6th Doctor","Colin Baker");
        DoctorWhos.put("7th Doctor","Sylvester McCoy");
        DoctorWhos.put("8th Doctor","Paul McGann");
        DoctorWhos.put("9th Doctor","Christopher Eccleston");
        DoctorWhos.put("10th Doctor","David Tennant");
        DoctorWhos.put("11th Doctor","Matt Smith");
        DoctorWhos.put("12th Doctor","Peter Capaldi");
        DoctorWhos.put("13th Doctor","Jodie Whittaker");

        return DoctorWhos.get(key);
    }

    /**
     * assertNotSame
     * AzureCertifications
     *
     * This is another Hash Map of the Azure Certifications I have left
     * to pass for my current job. The key is the certification number, and the
     * values are the actual name of the certification.
     **/
    public String AzureCertifications(final String key) {

        Map<String,String> AzureCertifications=new HashMap<>();
        AzureCertifications.put("AZ-104","Azure Administrator Associate");
        AzureCertifications.put("AZ-204","Azure Developer Associate");
        AzureCertifications.put("AZ-400","Azure DevOps Engineer");
        AzureCertifications.put("DP-300","Azure Database Administrator");

        return AzureCertifications.get(key);
    }

    /**
     * AssertNull
     * PassedCourses
     *
     * Here is a hashmap of the courses I
     * have taken in previous semesters. The key is the
     * course code and the value is the course
     * description.
     **/
    public String PassedCourses(final String key) {

        Map<String,String> PreviousCourses=new HashMap<>();
        PreviousCourses.put("CHEM101","Introductory Chemistry");
        PreviousCourses.put("REL225C","Foundations of the Restoration");
        PreviousCourses.put("CSE340","Web Backend Development I");
        PreviousCourses.put("CIT262","System Analysis and Design");
        PreviousCourses.put("COMM130","Visual Media");

        return PreviousCourses.get(key);
    }



}
