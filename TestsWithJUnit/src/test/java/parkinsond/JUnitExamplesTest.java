package parkinsond;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class JUnitExamplesTest {

    private final JUnitExamples examples = new JUnitExamples();
    @Test
    void useAssertThat() {
        assertThat("MyName",is("MyName"));
    }

    @Test
    void useAssertSame() {
        String firstValue = "8675309";
        String sameValue = "8675309";
        assertSame(firstValue, sameValue);
    }

    @Test
    void standardNumberAssertions() {
        int expected =12;
        int actual= examples.sumOfTwoNumbers(6, 6);
        assertEquals(expected, actual);
        assertFalse(examples.numberIsGreaterThan(3,4));
        assertTrue(examples.numberIsLessThan(10,100));
    }

    @Test
    void oregonCityArray() {
        List<String> oregoncities = examples.OregonCityArray();

        List<String> expectedOregonCities= new ArrayList<>();
        expectedOregonCities.add("Portland");
        expectedOregonCities.add("Newberg");
        expectedOregonCities.add("Salem");
        expectedOregonCities.add("Bend");
        expectedOregonCities.add("Gresham");
        expectedOregonCities.add("Tigard");

        assertArrayEquals(expectedOregonCities.toArray(),oregoncities.toArray());

    }

    @Test
    void theDoctors() {
        assertNotNull(examples.TheDoctors("1st Doctor"));
    }

    @Test
    void azureCertifications() {
        assertNotSame(examples.AzureCertifications("AZ-400"), examples.AzureCertifications("AZ-104"));
    }

    @Test
    void passedCourses() {
        assertNull(examples.PassedCourses("CIT365"));
    }
}