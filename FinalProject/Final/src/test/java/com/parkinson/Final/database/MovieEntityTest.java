package com.parkinson.Final.database;

import com.parkinson.Final.IsValidMovie;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.*;

public class MovieEntityTest {

    @Test void validMovieCreated() throws Exception
    {
        IsValidMovie validMovie = new IsValidMovie();
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        Boolean checkValidMovie = validMovie.validate(title, genre,rating,releaseYear);
        assertTrue(checkValidMovie);
    }
    @Test
    void getTitle() {
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        MovieEntity testMovie = new MovieEntity(title,genre,rating,releaseYear);
        assertNotNull(testMovie.getTitle());
    }

    @Test
    void setDifferentTitle() {
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        String title2 = "My Singles Ward";
        String genre2 = "Comedy";
        String rating2 = "PG";
        int releaseYear2 = 2000;

        MovieEntity testMovie = new MovieEntity(title,genre,rating,releaseYear);
        MovieEntity testMovie2 = new MovieEntity(title2,genre2,rating2,releaseYear2);
        assertNotSame(testMovie2.getTitle(),testMovie.getTitle());
    }

    @Test
    void getGenre() {
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        MovieEntity testMovie = new MovieEntity(title,genre,rating,releaseYear);
        assertNotNull(testMovie.getGenre());
    }

    @Test
    void setSameGenre() {
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        String title2 = "My Singles Ward";
        String genre2 = "Comedy";
        String rating2 = "PG";
        int releaseYear2 = 2000;

        MovieEntity testMovie = new MovieEntity(title,genre,rating,releaseYear);
        MovieEntity testMovie2 = new MovieEntity(title2,genre2,rating2,releaseYear2);
        assertSame(testMovie2.getGenre(),testMovie.getGenre());
    }

    @Test
    void getRating() {
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        MovieEntity testMovie = new MovieEntity(title,genre,rating,releaseYear);
        assertNotNull(testMovie.getRating());
    }

    @Test
    void setSameRating() {
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        String title2 = "My Singles Ward";
        String genre2 = "Comedy";
        String rating2 = "PG";
        int releaseYear2 = 2000;

        MovieEntity testMovie = new MovieEntity(title,genre,rating,releaseYear);
        MovieEntity testMovie2 = new MovieEntity(title2,genre2,rating2,releaseYear2);
        assertSame(testMovie2.getRating(),testMovie.getRating());
    }

    @Test
    void getReleasedyear() {
        String title = "Singles Ward";
        String genre = "Comedy";
        String rating = "PG";
        int releaseYear = 2000;

        String title2 = "My Singles Ward";
        String genre2 = "Comedy";
        String rating2 = "PG";
        int releaseYear2 = 2003;

        MovieEntity testMovie = new MovieEntity(title,genre,rating,releaseYear);
        MovieEntity testMovie2 = new MovieEntity(title2,genre2,rating2,releaseYear2);
        assertNotSame(testMovie2.getReleasedyear(),testMovie.getReleasedyear());
    }
}