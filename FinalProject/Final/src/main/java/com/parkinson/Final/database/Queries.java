package com.parkinson.Final.database;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;

public class Queries {

    SessionFactory factory = null;
    Session session = null;

    private static Queries single_instance = null;

    private Queries() {
        factory = HibernateUtils.getSessionFactory();
    }

    public static Queries getInstance() {
        if (single_instance == null) {
            single_instance = new Queries();
        }

        return single_instance;
    }

    public List<MovieEntity> getMovies() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            List<MovieEntity> movies = (List<MovieEntity>) session.createQuery("from MovieEntity").list();
            return movies;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public MovieEntity createMovie(String title,String genre,String rating, int releasedyear) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            MovieEntity newMovie = new MovieEntity();
            newMovie.setTitle(title);
            newMovie.setGenre(genre);
            newMovie.setRating(rating);
            newMovie.setReleasedyear(releasedyear);

            session.persist(newMovie);
            return  newMovie;

        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public void deleteMovie (String id)
    {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.remove((MovieEntity) session.find(MovieEntity.class, Integer.parseInt(id)));
            // Commit to DB
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }
}
