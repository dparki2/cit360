package com.parkinson.Final;

import java.io.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "registerAMovie", value = "/register-a-movie")
public class RegisterAMovieForm extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html>" +
                "<head>" +
                "<style>" +
                "* { font-size: 18px; }" +
                "h1 { font-size: 32px; }" +
                "form { width: 100%; max-width: 300px; }" +
                "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                "display: inline-flex;\n" +
                "justify-content: center;\n" +
                "align-items: center; color: #fff !important; }" +
                "button { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #fff; border: 1px solid #323232;\n" +
                "display: inline-flex;\n" +
                "justify-content: center;\n" +
                "align-items: center; color: #323232 !important; }" +
                "label { display: block; }" +
                "input, select { margin: 12px 0 20px 0; width: 100%; }" +
                "</style>" +
                "</head>" +
                "<body>");
        out.println("<h1>Register your Movie</h1>");
        out.println("<form action='CreateMovie' method='post'>");
        // Title
        out.println("<br/><label for='title'>Title: </label><input type='text' name='title' id='title'/>");
        // Genre
        out.println("<br/><label for='genre'>Genre: </label>" +
                "  <select name='genre' id='genre'>" +
                "  <option value='Action'>Action</option>" +
                "  <option value='Adventure'>Adventure</option>" +
                "  <option value='Comedy'>Comedy</option>" +
                "  <option value='Drama'>Drama</option>" +
                "  <option value='Documentary'>Documentary</option>" +
                "  <option value='Thriller'>Thriller</option>" +
                "</select>");
        // Rating
        out.println("<br/><label for='rating'>Rating: </label>" +
                "  <select name='rating' id='rating'>" +
                "  <option value='G'>G</option>" +
                "  <option value='PG'>PG</option>" +
                "  <option value='PG-13'>PG-13</option>" +
                "  <option value='R'>R</option>" +
                "</select>");
        // Release Year
        out.println("<br/><label for='releaseyear'>Release Year: </label><input type='text' name='releaseyear' id='releaseyear'/>");
        // Submit
        out.println("<button type='submit'>Save</button>");
        out.println("</form>");
        out.println("<a href=\"movie-list\">Movie List</a>\n" +
                "<a href=\"\\Final_war_exploded\">Back Home</a>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("The POST method is not valid for this servlet. Get method must be used.");
    }

    public void destroy() {
    }
}
