package com.parkinson.Final;

import com.parkinson.Final.database.MovieEntity;
import com.parkinson.Final.database.Queries;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.hibernate.JDBCException;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.temporal.ValueRange;
import java.util.List;

@WebServlet(name = "deleteMovie", value = "/delete-movie")
public class DeleteMovie extends  HttpServlet {

    private String message;

    public void init() {
        message = "Delete a movie";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try {
            Queries q = Queries.getInstance();
            String id = request.getParameter("id").toString();
            q.deleteMovie(id);

            out.println("<html>" +
                    "<head>" +
                    "<style>" +
                    "* { font-size: 18px; }" +
                    "h1 { font-size: 32px; }" +
                    "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                    "display: inline-flex;\n" +
                    "justify-content: center;\n" +
                    "align-items: center; color: #fff !important; }" +
                    "form { width: 100%; max-width: 300px; }" +
                    "label { display: block; }" +
                    "input, select { margin: 12px 0 20px 0; width: 100%; }" +
                    "</style>" +
                    "</head>" +
                    "<body>");
            out.println("<h1>Movie Deletion</h1>");
            out.println("<p>Movie Deleted</p>");
            out.println("<a href=\"register-a-movie\">Create a Movie</a>\n" +
                    "<a href=\"movies-list\">View Movies</a> " +
                    "<a href=\"\\Final_war_exploded\">Back Home</a>");
            out.println("</body></html>");
        } catch (JDBCException e) {
            out.println("<html><head><style>a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;" +
                    "display: inline-flex;" +
                    "justify-content: center; " +
                    "align-items: center; color: #fff !important; }</style>" +
                    "</head><body><p>Something went wrong, try to delete your movie again</p><a href=\"movies-list\">View Movies</a></body></html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("The POST method is not valid for this servlet. Get method must be used.");
    }
}
