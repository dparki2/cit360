package com.parkinson.Final;

import com.parkinson.Final.database.MovieEntity;
import com.parkinson.Final.database.Queries;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.hibernate.JDBCException;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.temporal.ValueRange;
import java.util.List;

@WebServlet(name = "createMovie", value = "/CreateMovie")
public class CreateMovie extends HttpServlet {

    private String message;

    public void init() {
        message = "Hello World!";
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("The GET method is not valid for this servlet. POST method must be used.");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JDBCException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Queries q = Queries.getInstance();
        List<MovieEntity> movies = q.getMovies();
        try {
            String title = request.getParameter("title");
            String genre = request.getParameter("genre");
            String rating = request.getParameter("rating");
            int releaseyear = Integer.parseInt(request.getParameter("releaseyear"));

            IsValidMovie validMovie = new IsValidMovie();
            Boolean isValidMovie = validMovie.validate(title, genre, rating, releaseyear);
            if (isValidMovie) {
                MovieEntity myNewMovie = q.createMovie(title, genre, rating, releaseyear);

                out.println("<html>" +
                        "<head>" +
                        "<style>" +
                        "* { font-size: 18px; }" +
                        "h1 { font-size: 32px; }" +
                        "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                        "display: inline-flex;\n" +
                        "justify-content: center;\n" +
                        "align-items: center; color: #fff !important; }" +
                        "form { width: 100%; max-width: 300px; }" +
                        "label { display: block; }" +
                        "input, select { margin: 12px 0 20px 0; width: 100%; }" +
                        "</style>" +
                        "</head>" +
                        "<body>");
                out.println("<h1>Your Movie:</h1>");
                out.println("<p>Title: " + title + "</p>");
                out.println("<p>Genre: " + genre + "</p>");
                out.println("<p>Rating: " + rating + "</p>");
                out.println("<p>Release Year: " + releaseyear + "</p>");
                out.println("<br/>");
                out.println("<a href=\"register-a-movie\">Create a Movie</a>\n" +
                        "<a href=\"movies-list\">View Movies</a> " +
                        "<a href=\"\\Final_war_exploded\">Back Home</a>");
                out.println("</body></html>");
            } else {
                out.println("<html><head><style>a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;" +
                        "display: inline-flex;" +
                        "justify-content: center; " +
                        "align-items: center; color: #fff !important; }</style>" +
                        "</head><body><p>Something went wrong, register your movie again. Be sure to fill out the entire form</p><a href=\"register-a-movie\">Register a Movie</a></body></html>");
            }
        } catch (JDBCException e) {
            out.println("<html><head><style>a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;" +
                    "display: inline-flex;" +
                    "justify-content: center; " +
                    "align-items: center; color: #fff !important; }</style>" +
                    "</head><body><p>Something went wrong, create your movie again. Be sure to fill out the entire form</p><a href=\"\\Final_war_exploded\">Back Home</a></body></html>");
        } catch (NumberFormatException e) {
            out.println("<html><head><style>a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;" +
                    "display: inline-flex;" +
                    "justify-content: center; " +
                    "align-items: center; color: #fff !important; }</style>" +
                    "</head><body><p>Something went wrong, create your Movie again. Be sure to fill the entire form</p><a href=\"\\Final_war_exploded\">Back Home</a></body></html>");
        }
    }
}
