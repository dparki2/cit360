package com.parkinson.Final.database;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "movies", schema="my_movies", catalog = "")
public class MovieEntity {

    private int id;
    private String title;
    private String genre;
    private String rating;
    private int releasedyear;

    public MovieEntity(String title, String genre, String rating, int releasedyear) {
        this.title = title;
        this.genre = genre;
        this.rating = rating;
        this.releasedyear = releasedyear;
    }

    public MovieEntity()
    {

    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "genre")
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Column(name = "rating")
    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Column(name = "releaseyear")
    public int getReleasedyear() {
        return releasedyear;
    }

    public void setReleasedyear(int releasedate) {
        this.releasedyear = releasedate;
    }

    @Override
    public String toString() {
        return  "\ntitle: " + title + '\'' +
                "\ngenre: " + genre + '\'' +
                "\nrating: " + rating + '\'' +
                "\nreleasedyear: " + releasedyear +
                "\nId: " + id;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        MovieEntity movie = (MovieEntity) obj;

        if (id != movie.id) return false;
        if (title != null ? !title.equals(movie.title) : movie.title != null) return false;
        if (genre != null ? !genre.equals(movie.genre) : movie.genre != null) return false;
        if (rating != null ? !rating.equals(movie.rating) : movie.rating != null) return false;
        if (releasedyear != movie.releasedyear) return false;

        return true;
    }

    @Override
    public int hashCode() { return Objects.hash(id, title, genre, rating, releasedyear); }

    public String toHTML() {
        return "\n<p>Title: <span class='detail'>" + title + "</span></p>" +
                "\n<p>Genre: <span class='detail'>" + genre + "</span></p>" +
                "\n<p>Rating: <span class='detail'>" + rating + "</span></p>" +
                "\n<p>Release Date: <span class='detail'>" + releasedyear + "</span></p>" +
                "\n<p>Id: <span class='detail'>" + id + "</span></p>";
    }

}

