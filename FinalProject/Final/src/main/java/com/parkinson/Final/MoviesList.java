package com.parkinson.Final;

import com.parkinson.Final.database.MovieEntity;
import com.parkinson.Final.database.Queries;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "moviesList", value = "/movies-list")
public class MoviesList extends HttpServlet {

    private String message;

    public void init() {
        message = "Welcome to your list of movies!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        Queries q = Queries.getInstance();
        List<MovieEntity> movies = q.getMovies();
        int len = movies.size();
        String _length = String.valueOf(len);
        System.out.println(len);

        out.println("<html>" +
                "<head>" +
                "<style>" +
                "* { font-size: 18px; }" +
                "h1 { font-size: 32px; }" +
                "form { width: 100%; max-width: 300px; }" +
                "label { display: block; }" +
                "input, select { margin: 12px 0 20px 0; width: 100%; } " +
                "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                "display: inline-flex;\n" +
                "justify-content: center;\n" +
                "align-items: center; color: #fff !important; }" +
                ".count { margin-left: 16px; }" +
                ".movie-container { display: flex; flex-direction: row; flex-wrap: wrap; }" +
                ".movie { max-width: 100%; width: 300px; display: flex; flex-direction: column; background-color: #ededed; padding: 16px; margin: 16px; border-radius: 8px;\n" +
                "box-shadow: 1px 2px 5px #767676;} " +
                ".movie p { color: #767676; } " +
                ".movie p .detail { color: #111; } " +
                "</style>" +
                "</head>" +
                "<body><p class='count'>Total Movies: <b>&nbsp;" + _length + "</b></p>");

        // open character container
        out.println("<div class='movie-container'>");

        for (MovieEntity m: movies) {
            out.println("<div class='movie'>" + m.toHTML() +
                    "<a href=\"./delete-movie?id=" + m.getId() + "\">Delete</a>" +
                    "</div>");
        }

        out.println("<a href=\"register-a-movie\">Register a Movie</a>\n" +
                "<a href=\"\\Final_war_exploded\">Back Home</a>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("The POST method is not valid for this servlet. Get method must be used.");
    }

}
