CREATE database my_movies;

--
-- Table structure for table `movies`
--
USE my_movies;

DROP TABLE IF EXISTS `movies`;
CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `genre` varchar(30) NOT NULL,
  `rating` varchar(5) NOT NULL,
  `releaseyear` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `movies` (`id`, `title`, `genre`, `rating`, `releaseyear`) VALUES
(1, 'The Fighting Preacher', 'Comedy','PG', '2019'),
(2, 'Other Side of Heaven', 'Drama','PG', '2002'),
(3, 'The RM', 'Comedy','PG', '2003'),
(4, 'Meet the Mormons', 'Documentary','G', '2014');

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);
  
--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;  