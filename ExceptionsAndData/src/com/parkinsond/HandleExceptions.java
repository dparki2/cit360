package com.parkinsond;

import java.util.InputMismatchException;
import java.util.Scanner;

public class HandleExceptions {
    private static Scanner keyboard = new Scanner(System.in);
    private static HandleExceptions inputValidator = new HandleExceptions();

    public static void main(String[] args) {
        // Demonstrates use of try catch statements
        runWithExceptionHandling();
        runWithDataValidation();

    }

    public static void runWithExceptionHandling()
    {
        boolean invalidInputs = true;
        do {

            System.out.println("***************************");
            System.out.print("This program will attempt to divide two numbers with exception handling.");
            System.out.println();
            try {
                System.out.printf("\nEnter the first number (numerator): ");
                float num = keyboard.nextFloat();
                System.out.printf("\nEnter the second number (denominator): ");
                float denomNum = keyboard.nextFloat();
                float quo = inputValidator.doDivision(num,denomNum);
                System.out.printf("\nDivision Result = " + quo);
                invalidInputs = false;
            }catch (InputMismatchException mismatchex){
                System.err.println("Sorry, your input does not match the expected input. Try again.");
                //keyboard.nextLine();
                System.err.println("Enter only numbers.");
            }catch(ArithmeticException arithEx){
                System.err.println("Please enter a non-zero denominator. Try again.");
                //arithEx.printStackTrace();
            }
            finally {
                System.out.println("\nThank you for using this program.");
                System.out.println("***************************");
                System.out.println();
            }
        }while(invalidInputs);
    }

    public static void runWithDataValidation(){
        boolean invalidInputs = true;

        do {

            System.out.println("***************************");
            System.out.print("This program will attempt to divide two numbers with data validation.");
            System.out.println();
            try {
                float num = inputValidator.askInputFloat("\nEnter the first number (numerator): ");
                float denomNum = inputValidator.askInputFloat("\nEnter the second number (denominator): ");
                if (denomNum == 0) {
                    System.err.println("Please enter a non-zero denominator. Try again.");
                }else {
                    float result = inputValidator.doDivision(num, denomNum);
                    System.out.printf("\nDivision Result = " + result);
                    invalidInputs = false;
                }
            } catch (InputMismatchException mismatchex){
                System.err.println("Sorry, your input does not match the expected input. Try again.");
                //keyboard.nextLine();
                System.err.println("Enter only numbers.");
            } catch(ArithmeticException arithEx){
                System.err.println("Please enter a non-zero denominator. Try again.");
                //arithEx.printStackTrace();
            }
            finally {
                System.out.println("\nThank you for using this program.");
                System.out.println("***************************");
                System.out.println();
            }
        }while(invalidInputs);
    }
    public float doDivision(float num1, float num2) throws  ArithmeticException {
        if (num2 == 0)
        {
            throw new ArithmeticException();
        }
        return  num1/num2;
    }

    /**
     * A method to repeatedly ask the user for input until
     * the input is valid. If condition is used,
     * input is measured against it.
     *
     * @param informationText	The information text to prompt
     * 							to the user.
     * @return					Returns the final value of the accepted
     * 							input, as a float.
     */
    public float askInputFloat(String informationText) {
        Boolean error = false;
        String userInp = "";
        do {
            System.out.print(informationText);
            userInp = keyboard.nextLine();
            // validate:
            if (!this.isType(userInp, "float")) {
                System.err.println("Error: Enter only numbers");
                error = true;
            } else {
                error = false;
            }

        } while (error == true);
        return Float.parseFloat(userInp);
    }

    /**
     * Tests if a specific input can be converted to a specific type.
     *
     * @param testStr The input to test. Accepts String, int, double or float.
     * @param type	Which type to test against. Accepts 'int','float' or 'double'.
     * @return Boolean	True if can be transformed to requested type. False otherwise.
     */
    public Boolean isType(String testStr, String type) {
        try {
            if (type.equalsIgnoreCase("float")) {
                Float.parseFloat(testStr);
            } else if (type.equalsIgnoreCase("int")) {
                Integer.parseInt(testStr);
            } else if (type.equalsIgnoreCase("double")) {
                Double.parseDouble(testStr);
            }
            return true;
        } catch(Exception e) {
            return false;
        }

    }
}
