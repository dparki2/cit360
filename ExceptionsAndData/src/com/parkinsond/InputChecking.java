package com.parkinsond;

import java.util.Scanner;
import java.util.regex.Pattern;

public class InputChecking {
    private static Scanner keyboard = new Scanner(System.in);
    private static String strPattern = "^\\d{5}(-\\d{4})?$";
    private static final Pattern zipPattern = Pattern.compile(strPattern);

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        // Check for valid input from user
        InputChecking inputValidator = new InputChecking();
        int personnelNumber = inputValidator.validIntInput("Please type your ID number: ");
        double age =  inputValidator.askInputDouble("Please type your age: ");

        System.out.println("A floating point number looks like 2.6f:");
        float myFloatNum = inputValidator.askInputFloat("Please type a floating number: " );
        System.out.println("Accepted Results:");
        System.out.println("ID #: " + personnelNumber);
        System.out.println("Age: " + age);
        System.out.println("Floating number #: " + myFloatNum);

        // Check for input types
        Boolean validAnswer;
        validAnswer = inputValidator.isType("10", "float"); //will return true;
        validAnswer = inputValidator.isType("1.0", "float"); //will return true;
        validAnswer = inputValidator.isType("blah", "int"); //will return false;

        String badZipCode = "980";

        try
        {
            inputValidator.validatePostalCode(badZipCode);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * A method to determine if the zipcode provided
     * is valid.
     *
     * @param zipProvided	    The zipcode information provided
     * 							by the user.
     */
    public void validatePostalCode(String zipProvided) {
        try {
            String zipCode = zipProvided;
            if (!zipPattern.matcher(zipCode).matches()) {
                throw new InvalidStatementException("Improper zipcode format.");
            }
            // do what you want here, after its been validated ..
        } catch (InvalidStatementException e) {
            System.err.println("Error: must be a valid US zipcode. " + e.getMessage());
            ;
        }
    }

    /**
     * A method to repeatedly ask the user for input until
     * the input is valid. If condition is used,
     * input is measured against it.
     *
     * @param informationText	The information text to prompt
     * 							to the user.
     * @return					Returns the final value of the accepted
     * 							input, as an integer.
     */
    public int validIntInput(String informationText) {
        Boolean error = false;
        String userInp = "";
        do {
            System.out.print(informationText);
            userInp = keyboard.nextLine();
            if (!this.isType(userInp, "int")) {
                error = true;
                System.err.println("Error: must be a whole number.");
            } else {
                error = false;
            }
        } while (error == true);
        return Integer.parseInt(userInp);
    }

    /**
     * A method to repeatedly ask the user for input until
     * the input is valid. If condition is used,
     * input is measured against it.
     *
     * @param informationText	The information text to prompt
     * 							to the user.
     * @return					Returns the final value of the accepted
     * 							input, as a double.
     */
    public double askInputDouble(String informationText) {
        Boolean error = false;
        String userInp = "";
        do {
            System.out.print(informationText);
            userInp = keyboard.nextLine();
            if (!this.isType(userInp, "double")) {
                System.err.println("Error: must be a number.");
                error = true;
            } else {
                error = false;
            }

        } while (error);
        return Double.parseDouble(userInp);
    }

    /**
     * A method to repeatedly ask the user for input until
     * the input is valid. If condition is used,
     * input is measured against it.
     *
     * @param informationText	The information text to prompt
     * 							to the user.
     * @return					Returns the final value of the accepted
     * 							input, as a float.
     */
    public float askInputFloat(String informationText) {
        Boolean error = false;
        String userInp = "";
        do {
            System.out.print(informationText);
            userInp = keyboard.nextLine();
            // validate:
            if (!this.isType(userInp, "float")) {
                System.err.println("Error: must be a number.");
                error = true;
            } else {
                error = false;
            }

        } while (error == true);
        return Float.parseFloat(userInp);
    }

    /**
     * Tests if a specific input can be converted to a specific type.
     *
     * @param testStr The input to test. Accepts String, int, double or float.
     * @param type	Which type to test against. Accepts 'int','float' or 'double'.
     * @return Boolean	True if can be transformed to requested type. False otherwise.
     */
    public Boolean isType(String testStr, String type) {
        try {
            if (type.equalsIgnoreCase("float")) {
                Float.parseFloat(testStr);
            } else if (type.equalsIgnoreCase("int")) {
                Integer.parseInt(testStr);
            } else if (type.equalsIgnoreCase("double")) {
                Double.parseDouble(testStr);
            }
            return true;
        } catch(Exception e) {
            return false;
        }

    }
}
