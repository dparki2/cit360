package com.parkinsond;

import java.util.Comparator;
import java.util.Objects;

public class Employee implements Comparator<Employee> {
    private String name;
    private String email;
    private String office;
    private String employeeId;

    public Employee(String name, String email, String office, String employeeId) {
        this.name = name;
        this.email = email;
        this.office = office;
        this.employeeId = employeeId;
    }

    public Employee(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String toString(){
        return "Name: " + name + " Email: " + email + " Office: " + office + " EmployeeId: " + employeeId;
    }

    @Override
    public int compare(Employee e1, Employee e2) {
        String employeeId1=e1.getEmployeeId();

        String employeeId2=e2.getEmployeeId();

        return employeeId1.compareTo(employeeId2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return email.equals(employee.email) && employeeId.equals(employee.employeeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, employeeId);
    }
}
