package com.parkinsond;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

public class JsonHttpServer {
    public  static void main(String[] args) throws Exception {


        HttpServer server = HttpServer.create(new InetSocketAddress(8950),2);
        server.createContext("/employee", new MyEmployeeHandler());
        server.createContext("/myemployees", new MyEmployeesHandler());
        server.setExecutor(null);
        server.start();
    }

    static class MyEmployeesHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            // Setup a list of employees to be able to use
            Employee emp1 = new Employee ("Drew", "dparkinson@myemail.com", "USA", "001");
            Employee emp2 = new Employee ("Chris", "cbangs@myemail.com", "USA", "002");
            Employee emp3 = new Employee ("Ron", "rdennis@myemail.com", "Mexico", "003");

            List<Employee> employeeList = new ArrayList<>();
            employeeList.add(emp1);
            employeeList.add(emp2);
            employeeList.add(emp3);

            String response = JSONHelper.employeesToJSON(employeeList);

            if("GET".equals(exchange.getRequestMethod())) {
                exchange.getResponseHeaders().set("Content-Type", "application/json");
                exchange.sendResponseHeaders(200, response.length());
                OutputStream responseStream = exchange.getResponseBody();
                responseStream.write(response.getBytes());
                responseStream.close();
            }
        }
    }

    static class MyEmployeeHandler implements HttpHandler{

        @Override
        public void handle(HttpExchange exchange) throws IOException {

            // Setup a list of employees to be able to use
            Employee emp1 = new Employee ("Drew", "dparkinson@myemail.com", "USA", "001");
            String response = JSONHelper.employeeToJSON(emp1);
                  if("GET".equals(exchange.getRequestMethod())) {
                exchange.getResponseHeaders().set("Content-Type", "application/json");
                exchange.sendResponseHeaders(200, response.length());
                OutputStream responseStream = exchange.getResponseBody();
                responseStream.write(response.getBytes());
                responseStream.close();
            }
        }
    }
}
