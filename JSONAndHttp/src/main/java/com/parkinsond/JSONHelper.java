package com.parkinsond;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class JSONHelper {


    public static String employeeToJSON(Employee employee) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String employeeData = "";
        try {
            employeeData = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(employee);
                    //mapper.writeValueAsString(employee);

        }
        catch (JsonProcessingException ex) {
            System.err.println(ex.toString());
            throw  ex;
        }

        return  employeeData;
    }

    public static Employee JSONtoEmployee(String jsonData) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Employee employee = null;
        //employee = mapper.readValue(jsonData,Employee.class);

       try {
            employee = mapper.readValue(jsonData,Employee.class);
        }
        catch (JsonProcessingException ex){
            System.err.println("Cannot map to Employee object");
            System.err.println(ex.toString());
            throw ex;
        }

        return  employee;
    }

    public static List<Employee> JSONtoEmployees(String jsonData) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        List<Employee> employeeList = mapper.readValue(jsonData, new TypeReference<List<Employee>>(){});

        return  employeeList;
    }

    public static String employeesToJSON(List<Employee> employees) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String employeeData = "";

        employeeData = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(employees);

        return  employeeData;

    }

}
