package com.parkinsond;


import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletionException;

public class JsonHttpClient {
    // Only use one HttpClient instance
    private static HttpClient client;

    public static void main(String[] args)  {
        GetSingleEmployee();
        GetAllEmployees();
    }

    private static void GetSingleEmployee() {
        try {
            String URI = "http://localhost:8950/employee";
            String basicResponse = getEmployeeData(URI);
            if (basicResponse.contains("<"))
            {
                System.err.println("JSON response was not received");
            }
            else {
                System.out.println("\nUsing call for JSONtoEmployee");
                Employee myEmployee = JSONHelper.JSONtoEmployee(basicResponse);

                System.out.println(myEmployee.toString());
            }
        } catch (IllegalArgumentException iae){
            System.err.println("The url provided did not resolve correctly");
        } catch (CompletionException e){
            System.err.println("The server was not able to be contacted");
        } catch (IOException e){
            System.err.println("The server could not read the response from the server");
        } catch (Exception e){
            System.err.println(e.toString());
        }
    }
    private static void GetAllEmployees() {
        try {
            String URI = "http://localhost:8950/myemployees";
            String basicResponse = getEmployeeData(URI);
            if (basicResponse.contains("<"))
            {
                System.err.println("JSON response was not received");
            }
            else {
                System.out.println("\nUsing call for JSONtoEmployees to get multiple employees");
                List<Employee> myEmployees = JSONHelper.JSONtoEmployees(basicResponse);
                for (Employee employee : myEmployees) {
                    System.out.println(employee.toString());
                }
            }

        } catch (IllegalArgumentException iae){
            System.err.println("The url provided did not resolve correctly");
        } catch (CompletionException e) {
            System.err.println("The server was not able to be contacted");
        } catch (IOException e){
            System.err.println("The server could not read the response from the server");
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    public static String getEmployeeData(String uri) throws IllegalArgumentException {

         client = HttpClient.newBuilder()
        .followRedirects(HttpClient.Redirect.NORMAL)
                 .connectTimeout(Duration.ofSeconds(3))
                 .build();

        HttpRequest request = HttpRequest.newBuilder(URI.create(uri))
                .timeout(Duration.ofSeconds(5))
                .GET()
                .build();

        return client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .join();
    }

}
