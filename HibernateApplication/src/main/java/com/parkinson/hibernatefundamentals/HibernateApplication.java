package com.parkinson.hibernatefundamentals;

import com.parkinson.hibernatefundamentals.airport.Airport;
import org.hibernate.HibernateException;

import javax.persistence.*;
import java.util.List;

public class HibernateApplication {
    public static void main(String[] args) {

        //Purpose of this program
        System.out.println("This program will demonstrate adding, listing, editing and deleting \n" +
                "content in a database as it relates to Airports. \n" +
                "This program will show how Hibernate issues commands to the database based on\n" +
                "what command is issued in code.\n");

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernatefundamentals.demo.app01");
        EntityManager em = emf.createEntityManager();

        try {

            System.out.println("Create 3 airports \n");
            em.getTransaction().begin();
            Airport airport = new Airport("PDX", "Portland International Airport");
            Airport airport2 = new Airport("PHX", "Phoenix International Airport");
            Airport airport3 = new Airport("SLC", "Salt Lake City International Airport");

            em.persist(airport);
            em.persist(airport2);
            em.persist(airport3);

            // Commit them to the database
            em.getTransaction().commit();

            System.out.println("\nQuery all airports \n");
            Query airportQuery = em.createQuery("SELECT a FROM Airport a");
            List<Airport> airports = (List<Airport>) airportQuery.getResultList();

            for (Airport air : airports) {
                System.out.println(air.toString());
            }

            em.getTransaction().begin();

            System.out.println("\nUpdate an airport \n");
            Airport updated_airport = em.find(Airport.class, 3);
            System.out.println(updated_airport.toString());
            updated_airport.setAirportcode("LAX");
            updated_airport.setName("Los Angeles International Airport");

            // Commit the update to the database
            em.getTransaction().commit();
            System.out.println(updated_airport.toString());

            System.out.println("\nDelete an airport \n");
            em.getTransaction().begin();

            updated_airport = em.find(Airport.class, 2);
            em.remove(updated_airport);

            em.getTransaction().commit();
            airports = null;

            System.out.println("\nGet the remaining airport values \n");
            airports = (List<Airport>) airportQuery.getResultList();

            for (Airport air : airports) {
                System.out.println(air.toString());
            }
            emf.close();
        }
        catch (Exception e)
        {
            em.getTransaction().rollback();
        }

    }

    /* The following are methods that could be used to perform the above functions.
     They were not used because I am creating the table each time I use the EntityManagerFactory
     based on the hibernate.hbm2ddl.auto property value in my persistence.xml
     This was done to make it easier to run my code over and over without having to drop
     the database each time
     */
    public Integer addAirport(String airportCode, String airportName)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernatefundamentals.demo.app01");
        EntityManager em = emf.createEntityManager();
        Integer airportID = null;
        EntityTransaction tx = null;

        try {
            tx = em.getTransaction();
            tx.begin();
            Airport airport = new Airport(airportCode, airportName);

            em.persist(airport);
            tx.commit();

            airportID = airport.getId();
        }
        catch (HibernateException e)
        {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        finally
        {
            emf.close();
        }

        return  airportID;
    }

    public void updateAirport(String airportCode, String airportName)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernatefundamentals.demo.app01");
        EntityManager em = emf.createEntityManager();

        EntityTransaction tx = null;

        try {
            tx = em.getTransaction();
            tx.begin();

            System.out.println("\nUpdate an airport \n");
            Airport updated_airport = em.find(Airport.class, 3);
            System.out.println(updated_airport.toString());
            updated_airport.setAirportcode(airportCode);
            updated_airport.setName(airportName);

            // Commit the update to the database
            tx.commit();
            System.out.println(updated_airport.toString());
        }
        catch (HibernateException e)
        {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        finally
        {
            emf.close();
        }
    }

    public void removeAirport(int airportId)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernatefundamentals.demo.app01");
        EntityManager em = emf.createEntityManager();
        Integer airportID = null;
        EntityTransaction tx = null;

        try {
            tx = em.getTransaction();
            tx.begin();

            System.out.println("\nDelete an airport \n");
            Airport remove_airport = em.find(Airport.class, airportId);
            em.remove(remove_airport);

            tx.commit();
            emf.close();
        }
        catch (HibernateException e)
        {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        finally
        {
            emf.close();
        }
    }

    public void listAirports()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernatefundamentals.demo.app01");
        EntityManager em = emf.createEntityManager();

        Query airportQuery = em.createQuery("SELECT a FROM Airport a");
        List<Airport> airports = (List<Airport>)airportQuery.getResultList();

        for (Airport air: airports) {
            System.out.println(air.toString());
        }

    }
}
