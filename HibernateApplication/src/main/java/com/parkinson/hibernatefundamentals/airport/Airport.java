package com.parkinson.hibernatefundamentals.airport;

import javax.persistence.*;

@Entity
@Table(name = "AIRPORTS")
public class Airport {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public String getAirportcode() {
        return airportcode;
    }

    public void setAirportcode(String airportcode) {
        this.airportcode = airportcode;
    }

    @Column(name = "AIRPORTCODE")
    private String airportcode;

    @Column(name = "NAME")
    private String name;

    public Airport(String airportcode, String name) {
        //this.id = id;
        this.airportcode = airportcode;
        this.name = name;
    }

    public Airport() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", airportcode='" + airportcode + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
