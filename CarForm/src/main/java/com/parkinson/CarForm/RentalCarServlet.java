package com.parkinson.CarForm;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "RentalCarServlet", value = "/RentalCarServlet")
public class RentalCarServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>"
                        + "<html><head>"
                        + "<meta charset='UTF-8'>"
                        + "<title>Rental Car Servlet Demo</title>"
                        + "</head><body>");
        out.println("<h2>Rental Car Preferences:</h2>");
        out.println("<form method='post' action='RentalCarServlet'>");
        out.println("<input type='text' name='make' placeholder='Rental Car Make'>");
        out.println("<input type='text' name='model' placeholder='Car Model'>");
        out.println("<input type='number' name='mpg' placeholder='Minimum Miles Per Gallon'>");
        out.println("<button type='submit'>SUBMIT</button>");
        out.println("</form>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>"
                + "<html><head>"
                + "<meta charset='ISO-8859-1'>"
                + "<title>Rental Car Servlet Demo</title>"
                + "</head><body>");
        try
        {

        String make = request.getParameter("make");
        String model = request.getParameter("model");

        if (make.isEmpty() || model.isEmpty())
        {
            out.println("<h2>There was a problem requesting a Car rental!</h2>"
                    + "<h2>Your request could not be processed:</h2>");

            if (make.isEmpty())
            {
                out.println("<p>Make cannot be left blank</p>");
            }

            if (model.isEmpty())
            {
                out.println("<p>Model cannot be left blank</p>");
            }

            throw new IOException();
        }

        if (request.getParameter("mpg").isEmpty())
        {
            out.println("<h2>There was a problem requesting a Car rental!</h2>"
                    + "<h2>Your request could not be processed:</h2>");
            out.println("<p>MPG cannot be left blank</p>");
            out.println("</body></html>");
            throw new ServletException();
        }

        double mpg = Long.parseLong(request.getParameter("mpg"));

        RentalCar myRentalCar = new RentalCar(make,model,mpg);

        out.println("<h2>Thanks for requesting a Car rental!</h2>"
                + "<p>Rental information below was provided by a form.</p>"
                + "<h2>Your requested Car Rental:</h2>");
        out.println("<p>Car Make: " + myRentalCar.getMake() +
                "</p><p>Model: " + myRentalCar.getModel() + "</p><p>Miles Per Gallon: " +
                myRentalCar.getMpg());
        out.println("</body></html>");
        }
        catch (NumberFormatException n)
        {
            out.println("<h2>There was a problem requesting a Car rental!</h2>"
                    + "<h2>Your request could not be processed:</h2>");
            out.println("<p>MPG must be a number</p>");
            out.println("</body></html>");
        }
        catch (IOException n)
        {
            out.println("</body></html>");
        }
        catch (ServletException s)
        {
            out.println("</body></html>");
        }
        catch (Exception e)
        {
            response.sendRedirect("index.html");
        }
    }
}
